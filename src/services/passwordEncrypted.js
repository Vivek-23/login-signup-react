const bcrypt = require('bcrypt');

const encryptedPassword = async (password) => {
    try{
        const saltRounds = 10;
        const hash = await bcrypt.hash(password, saltRounds);
        return hash;
    }catch(err) {
        console.log(err);
        throw err;
    }
}

module.exports = encryptedPassword;