const User = require('../models/createTable');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const validateEmailPassword = async (email, password) => {
    try {
        let response = await User.findOne({
            where: {
                email: email
            }
        });
        if (response) {
            let user = response.get();
            let hashPassword = user.password;
            let matchPassword = await bcrypt.compare(password, hashPassword);
            if (!matchPassword) {
                throw Error('Password not matched')
            } else {
                let token = await jwt.sign(
                    {
                        id: user.userId
                    },
                    'jwtSecret',
                    {expiresIn: 3600}
                );

                return {token, 
                    user: {
                        id: user.userId,
                        name: user.name,
                        email: user.email
                    }}
            }
        } else {
            throw Error('Email not found');
        }
    } catch (err) {
        throw Error(err);
    }
}

module.exports = validateEmailPassword;