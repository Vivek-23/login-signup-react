const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    let token = req.header('Authorization');
    console.log(token);

    if(!token){
        return res.status(401)
            .json({
                message: "Unknown user authorization"
            });
    }else{
        try {
            const decoded = jwt.verify(token, 'jwtSecret');
            req.user = decoded;
            next();
        } catch (error) {
            return res.status(400)
                .json({
                    message: 'Token is not valid'
                });
        }
    }
}