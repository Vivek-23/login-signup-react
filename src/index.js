const express = require('express');
const cors = require('cors');
const insertUserData = require('./models/insertUserData');
const sequelize = require('./models/connectDb');
const encryptedPassword = require('./services/passwordEncrypted');
const validateEmailPassword = require('./services/validateEmailPassword');
const userAuth = require('./services/userAuthentication');
const User = require('./models/createTable');

const app = express();

app.use(cors());
app.use(express.json());

sequelize.sync()
    .catch((err) => {
        console.log(err);
    });

app.post('/signup', async (req, res) => {
    try {
        let name = req.body.name;
        let email = req.body.email;
        let password = await encryptedPassword(req.body.password);
        const response = await insertUserData(name, email, password);
        return res.status(200).json({
            status: 'Registered Successfully'
        });
    } catch (err) {
        return res.status(400)
            .json({
                status: "Bad Request"
            });
    }
});


app.post('/login', async (req, res) => {
    try {
        let email = req.body.email;
        let password = req.body.password;
        let validatePassword = await validateEmailPassword(email, password);
        return res.status(200)
            .json({
                message: 'Login Successfully',
                userDetails: validatePassword
            });
    } catch (error) {  
        return res.status(401)
            .json({
                message: error.message
            });
    }
});

app.get('/user', userAuth, async (req, res) => {
    try {
        const user = await User.findByPk(req.user.id);
        if(!user){
            throw Error('User doesnot exists');
        }else{
            res.status(200).json({
                id: user.userId,
                name: user.name,
                email: user.email
            });
        }
    } catch (error) {
        res.status(400).json({
            message: e.message
        });
    }
})

app.listen(3001, () => {
    console.log('Server is listening on 3001');
});