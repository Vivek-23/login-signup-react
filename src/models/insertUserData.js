const User = require('./createTable');
const sequelize = require('./connectDb');

const insertUserData = async (name, email, password) => {
    try {
        const create = await User.create({
            name: name,
            email: email,
            password: password
        });
        return create;
    }catch(err) {
        console.log(err);
    }
}

module.exports = insertUserData;