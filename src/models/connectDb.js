const {Sequelize} = require('sequelize');
const dotenv = require('dotenv');

dotenv.config();


const sequelize = new Sequelize({
    host: process.env.POSTGRES_SQL_HOST,
    username: process.env.POSTGRES_SQL_USER,
    password: process.env.POSTGRES_SQL_PASSWORD,
    database: process.env.POSTGRES_SQL_DATABASE,
    dialect: 'postgres'
});

async function connectDb() {
    try {
        await sequelize.authenticate();
        console.log('Connection successfully');
    }catch(err) {
        console.log('Unable to connect database: ' + err);
    }
}

module.exports = sequelize;