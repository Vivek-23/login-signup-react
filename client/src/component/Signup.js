import { Link, useHistory } from 'react-router-dom';
import React, { Component } from 'react'
import { Form, FormGroup, Alert } from 'reactstrap'
import './css/Signup.css';
import Axios from 'axios';

export default class Signup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            password: '',
            repassword: '',
            message: '',
            visible: false
        };


    }

    changeName = (event) => {
        this.setState({
            name: event.target.value
        });
    }
    changeEmail = (event) => {
        this.setState({
            email: event.target.value
        });
    }
    changePassword = (event) => {
        this.setState({
            password: event.target.value
        });
    }
    changeRepassword = (event) => {
        this.setState({
            repassword: event.target.value
        });
    }

    fetchUserDetails = async () => {
        let response = await Axios.post('/signup', {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password
        });

        return response;
    }


    handleSubmit = async (event) => {
        event.preventDefault();
        if (this.state.password !== this.state.repassword) {
            this.setState({
                message: 'Password are not matched',
                visible: true,
                password: '',
                repassword: ''
            });
        } else {
            try {
                let response = await this.fetchUserDetails();
                if (response.status === 200) {
                    this.setState({
                        message: 'Register Successfully',
                        visible: true
                    })
                }
            } catch (error) {
                console.log(error);
            }
        }
    }

    render() {
        const { visible, message} = this.state
        return (
            <div className="container">
                <Alert color="primary" isOpen={visible}>
                    {message}
                </Alert>
                <div className="signup-container">
                    <h1>Sign up</h1>
                    <Form className="signup-form" onSubmit={this.handleSubmit}>
                        <FormGroup>
                            <input id="standard-basic"
                                type="text"
                                onChange={this.changeName}
                                placeholder="Enter your name"
                                required
                            />
                        </FormGroup>
                        <FormGroup>
                            <input id="standard-basic"
                                type="email"
                                onChange={this.changeEmail}
                                required
                                placeholder="Enter email"
                            />
                        </FormGroup>
                        <FormGroup>
                            <input id="standard-basic"
                                type="password"
                                onChange={this.changePassword}
                                required
                                placeholder="Enter password"
                            />
                        </FormGroup>
                        <FormGroup>
                            <input id="standard-basic"
                                type="password"
                                onChange={this.changeRepassword}
                                required
                                placeholder="Retype Password"
                            />
                        </FormGroup>
                        <br />
                        <button id="btn" variant="contained" color="primary" type="submit">
                            Register
                        </button>
                        <br />
                        <div id="signup-link">
                            <Link to="/" id="signup-link">
                                I am already member
                            </Link>
                        </div>

                    </Form>
                </div>
            </div>
        )
    }
}
