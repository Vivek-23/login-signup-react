import React, { Component } from 'react'
import { Form, FormGroup, Alert } from 'reactstrap'
import { Link, useHistory } from 'react-router-dom';
import LoginImage from './image/signup-img.png';
import './css/login.css';
import Axios from 'axios';

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            message: '',
            visible: false
        };
    }

    emailChange = (event) => {
        console.log(event.target.value)
        this.setState({
            email: event.target.value
        });
    }

    passwordChange = (event) => {
        this.setState({
            password: event.target.value
        });
    }

    fetchUserDetails = async () => {
        let response = await Axios.post('http://localhost:3001/login', {
            email: this.state.email,
            password: this.state.password
        });

        return response;
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        try {
            let response = await this.fetchUserDetails();
            localStorage.setItem('token', response.data.userDetails.token);
            this.setState({
                message: 'Login Successfully',
                visible: true
            })
        } catch (err) {
            this.setState({
                message: 'Invalid Email and Password',
                visible: true
            })
        }
    }

    render() {
        const { visible, message, email, password } = this.state
        return (
            <div className="container">
                <Alert color="primary" isOpen={visible}>
                    {message}
                </Alert>
                <div className="login-container">
                    <h1>Login</h1>
                    <Form className="login-form" onSubmit={this.handleSubmit}>
                        <FormGroup>
                            <input id="standard-basic"
                                type="email"
                                onChange={this.emailChange}
                                required
                                placeholder="Enter email"
                            />
                        </FormGroup>
                        <FormGroup>
                            <input id="standard-basic"
                                type="password"
                                onChange={this.passwordChange}
                                required
                                placeholder="Enter password"
                            />
                        </FormGroup>
                        <button id="login-btn" variant="contained" color="primary" type="submit">
                            Login
                        </button>
                        <div id="signup-link">
                            Doesn't have an account?
                            <br />
                            <Link to="/signup" id="login-link">
                                Signup Here
                            </Link>
                        </div>
                    </Form>
                </div>
            </div>
        )
    }
}

export default Login
